# Test Case Frontent

## Pengerjaan menggunakan vue-cli

## Login menggunakan api berikut 
> https://api.halalnode.com:8250/v1/accounts/login 

**Request Method: POST**

identity: "ruly"

password: "Qwerty123",

registrationToken: "",

admin: false,

staySignedIn: false



## Buat link menuju halaman yang menampilkan data dari api berikut
> https://api.halalnode.com:8250/v1/certification-body/landing?compact=true&page=1&limit=8 

**Request Method: GET**

Boleh menggunakan tabel atau yang lain


## Logout menggunakan api berikut
>  https://api.halalnode.com:8250/v1/accounts/logout

**Request Method: DELETE**

Headers: Bearer {token hasil dari login}